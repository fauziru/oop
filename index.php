<?php
    require 'Animal.php';
    require 'Ape.php';
    require 'Frog.php';

    $sheep = new Animal("Shawn");
    echo $sheep->get_name()."\n";
    echo $sheep->get_legs()."\n";
    echo $sheep->get_cold_blooded()."\n";

    $ape = new Ape("Wukong");
    echo $ape->get_name()."\n";
    echo $ape->yell()."\n";
    echo $ape->get_legs()."\n";

    $frog = new Frog("Prince");
    echo $frog->get_name()."\n";
    echo $frog->jump()."\n";
    echo $frog->get_legs()."\n";
?>